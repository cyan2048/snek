# WILL FULLY MIGRATE TO [GITHUB](https://github.com/cyan-2048/_store) SOON

The new store-db version requires apps to have separate download links for the manifest.webapp to handle version number changes... Because GitLab has CORS issues, I have decided to migrate to GitHub.